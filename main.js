// Fetch unordered list
const ul = document.querySelector("ul");

// Fetch text input
const text = document.querySelector("#newTask");

// Fetch Add button
const btnAdd = document.querySelector("#btnAdd");

// Fetch Error message container
const errorMsg = document.querySelector(".errorMsg");
errorMsg.style.display = "none";

const displayError = () => {

    errorMsg.style.display = "block";

    setTimeout(() => {

        errorMsg.style.display = "none";

    }, 2000);

}

// Add event listener to Add button
btnAdd.addEventListener('click', (e) => {

    e.preventDefault();

    if(text.value === ""){

        displayError();

        return;

    }

    const newTask = text.value;

    text.value = "";

    const newItem = {
        task: newTask,
        completed: false
    };

    let itemList = JSON.parse(localStorage.getItem("todo"));

    if(!itemList){

        itemList = [];

    }

    itemList.push(newItem);

    localStorage.setItem("todo", JSON.stringify(itemList));

    refreshList();

})

// Toggle Complete/Incomplete task
const toggleTask = (index) => {

    const listItems = JSON.parse(localStorage.getItem("todo"));

    if(listItems[index]["completed"] == true){

        listItems[index]["completed"] = false;

    }
    else if(listItems[index]["completed"] == false){

        listItems[index]["completed"] = true;

    }

    localStorage.setItem("todo", JSON.stringify(listItems));

    refreshList();

}

// Save Edited task
const saveTask = (index, ipText) => {

    const listItem = JSON.parse(localStorage.getItem("todo"));
    listItem[index]["task"] = ipText.value;

    localStorage.setItem("todo", JSON.stringify(listItem));

    refreshList();

}

// Edit task
const editTask = (index, btnContainer) => {

    while(btnContainer.firstChild){
        btnContainer.removeChild(btnContainer.firstChild);
    }

    // Create form element
    const newForm = document.createElement("form");
    btnContainer.append(newForm);

    // Create input text field
    const ipText = document.createElement("input");
    ipText.type = "text";
    ipText.value = JSON.parse(localStorage.getItem("todo"))[index]["task"];
    newForm.append(ipText);
    ipText.focus();

    // Create Save button
    const saveBtn = document.createElement("button");
    saveBtn.innerText = "SAVE";
    newForm.append(saveBtn);

    // Add event listener to save button
    saveBtn.addEventListener('click', (e) => {

        e.preventDefault();

        saveTask(index, ipText);

    })

}

// Remove task
const removeTask = (index) => {

    const listItems = JSON.parse(localStorage.getItem("todo"));

    const newList = [];
    for(let i = 0; i < listItems.length; i++){

        if(i != index){

            newList.push(listItems[i]);

        }

    }

    localStorage.setItem("todo", JSON.stringify(newList));

    refreshList();

}

const createListItem = (index) => {

    // Create unordered list item
    const li = document.createElement("li");
    ul.append(li);

    // Create container div for complete/incomplete button
    const completeBtnContainer = document.createElement("div");
    li.append(completeBtnContainer);

    // Create complete/incomplete button
    const btnComplete = document.createElement("button");
    completeBtnContainer.append(btnComplete);

    const itemList = JSON.parse(localStorage.getItem("todo"));
    
    // Create task block
    const task = document.createElement("div");
    li.append(task);
    task.innerText = `${itemList[index]["task"]}`;

    if(itemList[index]["completed"] == true){

        btnComplete.innerText = "TASK INCOMPLETE";

        task.style.textDecoration = "line-through";

    }
    else if(itemList[index]["completed"] == false){

        btnComplete.innerText = "TASK COMPLETE";

        task.style.textDecoration = "none";

    }

    // Add event listener to Complete/incomplete button
    btnComplete.addEventListener('click', (e) => {

        toggleTask(index);

    })

    // Create Edit and Remove button container
    const btnContainer = document.createElement("div");
    li.append(btnContainer);

    // Create Edit button
    const btnEdit = document.createElement("button");
    btnContainer.append(btnEdit);
    btnEdit.innerText = "EDIT";

    // Create Remove button
    const btnRemove = document.createElement("button");
    btnContainer.append(btnRemove);
    btnRemove.innerText = "REMOVE";

    // Add event listener to Edit button
    btnEdit.addEventListener('click', (e) => {

        e.preventDefault();

        editTask(index, btnContainer);

    })

    // Add event listener to Remove button
    btnRemove.addEventListener('click', (e) => {

        e.preventDefault();

        removeTask(index);

    })

}

const refreshList = () => {

    while(ul.firstChild){
        ul.removeChild(ul.firstChild);
    }

    // Fetch indices of tasks
    const indices = JSON.parse(localStorage.getItem("todo"));

    for(let index in indices){

        // Create task items one by one
        createListItem(index);

    }

}

refreshList();